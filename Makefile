.POSIX:
.PHONY: all clean example valgrind

CC=clang
CFLAGS=-std=c99 -lm -Werror -Wall -pedantic

all: bars

bars: bars.c
	$(CC) $(CFLAGS) -o $(@) $(^)

example: bars
	./bars -c 80 < example
	./bars example

debug: bars.c
	$(CC) $(CFLAGS) -g -o $(@) $(^)

valgrind: debug
	valgrind ./debug example

memcheck: bars.c
	$(CC) $(CFLAGS) \
		-O1 \
		-fno-omit-frame-pointer \
		-fsanitize=address \
		-o $(@) \
		$(^)
	./memcheck example

clean:
	rm bars example memcheck

